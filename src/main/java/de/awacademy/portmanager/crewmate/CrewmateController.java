package de.awacademy.portmanager.crewmate;

import de.awacademy.portmanager.PortService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class CrewmateController {
    private PortService portService;

    public CrewmateController(PortService portService){
        this.portService = portService;
    }

    @GetMapping("/new-crewmate-form")
    public String newCrewmateForm(Model model){
        model.addAttribute("crewmate", new CrewmateDTO());
        model.addAttribute("shipList", portService.getShipRepository().findAll());
        model.addAttribute("shipId",0);
        return "crewmate/new-crewmate-form";
    }

    @PostMapping("/add-crewmate")
    public String addCrewmate(@ModelAttribute CrewmateDTO crewmateDTO){
        int shipId = crewmateDTO.getShipId();
        Crewmate crewmate = new Crewmate();
        crewmate.setFirstName(crewmateDTO.getFirstName());
        crewmate.setHiddenTalent(crewmateDTO.getHiddenTalent());
        crewmate.setWorksOn(portService.getShipRepository().getReferenceById(shipId));
        portService.getCrewmateRepository().save(crewmate);
        return "redirect:/ship-detail" + "?shipId=" + shipId;
    }

    @PostMapping("/delete-crewmate")
    public String deleteCrewmate(@RequestParam int crewId){
        int shipId = portService.getCrewmateRepository().findById(crewId).get().getWorksOn().getId();
        portService.getCrewmateRepository().deleteById(crewId);
        return "redirect:/ship-detail" + "?shipId=" + shipId;
    }
}
