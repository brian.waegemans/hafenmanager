package de.awacademy.portmanager.crewmate;

public class CrewmateDTO {
    private String firstName;
    private String hiddenTalent;
    private int shipId;

    // Methods

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getHiddenTalent() {
        return hiddenTalent;
    }

    public void setHiddenTalent(String hiddenTalent) {
        this.hiddenTalent = hiddenTalent;
    }

    public int getShipId() {
        return shipId;
    }

    public void setShipId(int shipId) {
        this.shipId = shipId;
    }
}
