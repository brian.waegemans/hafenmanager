package de.awacademy.portmanager.crewmate;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CrewmateRepository extends JpaRepository<Crewmate, Integer> {
}
