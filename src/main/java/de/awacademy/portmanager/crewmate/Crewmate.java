package de.awacademy.portmanager.crewmate;

import de.awacademy.portmanager.ship.Ship;
import jakarta.persistence.*;

@Entity
public class Crewmate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String firstName;
    private String hiddenTalent;

    @ManyToOne
    private Ship worksOn;

    // Methods
    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getHiddenTalent() {
        return hiddenTalent;
    }

    public void setHiddenTalent(String hiddenTalent) {
        this.hiddenTalent = hiddenTalent;
    }

    public Ship getWorksOn() {
        return worksOn;
    }

    public void setWorksOn(Ship worksOn) {
        this.worksOn = worksOn;
    }
}
