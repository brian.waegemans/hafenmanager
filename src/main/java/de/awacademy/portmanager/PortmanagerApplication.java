package de.awacademy.portmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PortmanagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PortmanagerApplication.class, args);
    }

}
