package de.awacademy.portmanager;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PortController {

    private PortService portService;

    public PortController(PortService portService) {
        this.portService = portService;
    }

    @GetMapping("/")
    public String index(Model model){
        model.addAttribute("shipList", portService.getShipRepository().findAllByOrderByBuildYearDesc());
        return "index";
    }
}
