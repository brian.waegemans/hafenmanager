package de.awacademy.portmanager;

import de.awacademy.portmanager.crewmate.CrewmateRepository;
import de.awacademy.portmanager.ship.ShipRepository;
import org.springframework.stereotype.Service;

@Service
public class PortService {

    private ShipRepository shipRepository;
    private CrewmateRepository crewmateRepository;

    public PortService(ShipRepository shipRepository, CrewmateRepository crewmateRepository) {
        this.shipRepository = shipRepository;
        this.crewmateRepository = crewmateRepository;
    }

    public ShipRepository getShipRepository() {
        return shipRepository;
    }

    public CrewmateRepository getCrewmateRepository() {
        return crewmateRepository;
    }
}
