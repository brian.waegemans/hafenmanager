package de.awacademy.portmanager.ship;

public class ShipDTO {
    private String name;
    private int buildYear;
    private String imgLink;

    // Methods
    public String getName() {
        return name;
    }

    public void setName(String firstName) {
        this.name = firstName;
    }

    public int getBuildYear() {
        return buildYear;
    }

    public void setBuildYear(int hiddenTalent) {
        this.buildYear = hiddenTalent;
    }

    public String getImgLink() {
        return imgLink;
    }

    public void setImgLink(String imgLink) {
        this.imgLink = imgLink;
    }
}
