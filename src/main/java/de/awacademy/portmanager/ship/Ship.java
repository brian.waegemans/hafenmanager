package de.awacademy.portmanager.ship;

import de.awacademy.portmanager.crewmate.Crewmate;
import jakarta.persistence.*;

import java.util.List;

@Entity
public class Ship {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private int buildYear;
    private String imgLink;

    @OneToMany(mappedBy = "worksOn")
    private List<Crewmate> crewmateList;


    // Methods
    public String getName() {
        return name;
    }

    public void setName(String firstName) {
        this.name = firstName;
    }

    public int getBuildYear() {
        return buildYear;
    }

    public void setBuildYear(int hiddenTalent) {
        this.buildYear = hiddenTalent;
    }

    public int getId() {
        return id;
    }

    public List<Crewmate> getCrewmateList() {
        return crewmateList;
    }

    public String getImgLink() {
        return imgLink;
    }

    public void setImgLink(String imgLink) {
        this.imgLink = imgLink;
    }
}
