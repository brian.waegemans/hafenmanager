package de.awacademy.portmanager.ship;

import de.awacademy.portmanager.PortService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ShipController {
    private PortService portService;
    public ShipController(PortService portService){
        this.portService = portService;
    }

    @GetMapping("/new-ship-form")
    public String newShipForm(Model model){
        model.addAttribute("ship", new ShipDTO());
        return "ship/new-ship-form";
    }

    @PostMapping("/add-ship")
    public String addShip(@ModelAttribute ShipDTO shipDTO){
        Ship ship = new Ship();
        ship.setName(shipDTO.getName());
        ship.setBuildYear(shipDTO.getBuildYear());
        ship.setImgLink(shipDTO.getImgLink());
        portService.getShipRepository().save(ship);
        return "redirect:/";
    }

    @GetMapping("/ship-detail")
    public String shipDetail(Model model, @RequestParam int shipId){
        model.addAttribute("ship", portService.getShipRepository().findById(shipId).get());
        return "ship/ship-detail";
    }
}
