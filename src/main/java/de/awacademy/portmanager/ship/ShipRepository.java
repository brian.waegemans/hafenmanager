package de.awacademy.portmanager.ship;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ShipRepository extends JpaRepository<Ship, Integer> {
    List<Ship> findAllByOrderByBuildYearDesc();
}
